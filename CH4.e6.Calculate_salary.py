# Exercise 6: Rewrite your pay computation with time-and-a-half for over-time and create a function called compute pay which takes two parameters(hours and rate).

def computepay():
    pay = (hours * rate)
    print(pay)

try:
    hours = float(input("Enter hours per week here: "))
    rate = float(input("Enter your rate here: "))

    if hours > 40:
        overpay = (hours - 40) * 1.5 * rate
        total = overpay + (40 * rate)
        print (f"Your pay seems to be: {total}")
    else:
        computepay()
except:
    print("Error,please use numeric input")


