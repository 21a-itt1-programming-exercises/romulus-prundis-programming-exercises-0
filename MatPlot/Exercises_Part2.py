import numpy as np
from matplotlib import pyplot as plt
# print(plt.style.available) to check all the available styles, check terminal

# plt.xkcd()
plt.style.use("fivethirtyeight")

#first data set x and y, regular salaries
ages_x = [22, 24, 25, 33, 45, 46, 47, 48]

x_indexes = np.arange(len(ages_x))
width = 0.25

dev_y = [20000, 22000, 23000, 24000, 25000, 26000, 27000, 28000]



#second data set with the same age line
py_dev_y = [29100, 3200, 3300, 3400, 3500, 4600, 3700, 2900]
plt.plot(x_indexes, width = width,  py_dev_y,  label = "devs")  # color="#5a7d9a", linewidth= 3,
#plt.bar(x_indexes, dev_y, color="#444444", label="all dev")   >>for creating a bar chart instead of graph

js_dev_y = [35000, 40000, 41000, 35000, 46000, 28000, 30000, 31000]
plt.plot(x_indexes + width, js_dev_y, width = width,  label = "JavaScript")  # color="#adad3b", linewidth= 3,

#plt.plot allows us to plot the data, k is the black and -- is for  dash line
plt.plot(x_indexes, dev_y, width = width,  color="#444444", linestyle="--", label = "regular" ) #

plt.xlabel("Ages")
plt.ylabel("Medium saalry (DKK)")
plt.title("Median salary (DKK) by Age")

plt.legend()
# plt.grid(True)   for making a grid (squares) to see when it's crossing a salary

#making the padding better on all devices
plt.tight_layout()

#to save the image of the plot
plt.savefig("plot.png")

plt.show()

