from matplotlib import pyplot as plt
# print(plt.style.available) to check all the available styles, check terminal

# plt.xkcd()
plt.style.use("fivethirtyeight")

#first data set x and y, regular salaries
ages_x = [22, 24, 25, 33, 45, 46, 47, 48]
dev_y = [1000, 1200, 1300, 1400, 1500, 1600, 1700, 1800]



#second data set with the same age line
py_dev_y = [2100, 2200, 2300, 2400, 2500, 2600, 2700, 2900]
plt.plot(ages_x, py_dev_y,  label = "devs")  # color="#5a7d9a", linewidth= 3,
#plt.bar(ages_x, dev_y, color="#444444", label="all dev")   >>for creating a bar chart instead of graph

js_dev_y = [1500, 2000, 2100, 2500, 2600, 2800, 3000, 3100]
plt.plot(ages_x, js_dev_y,  label = "JavaScript")  # color="#adad3b", linewidth= 3,

#plt.plot allows us to plot the data, k is the black and -- is for  dash line
plt.plot(ages_x, dev_y,  color="#444444", linestyle="--", label = "regular" ) #

plt.xlabel("Ages")
plt.ylabel("Medium saalry (DKK)")
plt.title("Median salary (DKK) by Age")

plt.legend()
# plt.grid(True)   for making a grid (squares) to see when it's crossing a salary

#making the padding better on all devices
plt.tight_layout()

#to save the image of the plot
plt.savefig("plot.png")

plt.show()

