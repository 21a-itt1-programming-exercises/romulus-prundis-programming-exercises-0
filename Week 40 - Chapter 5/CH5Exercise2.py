'''
Exercise 2: Write another program that prompts for a list of numbers
as above and at the end prints out both the maximum and minimum of
the numbers instead of the average.
'''

numbers = []

while True:
    user = input("Enter a number: ")
    try:
        user = float(user)
        numbers.append(user)
    except:
        if user == "done" or "Done":
            break
        else:
            print("Please enter a number instead.")
smallest = min(numbers)
largest = max(numbers)
print(smallest, largest)
