'''
Exercise 1: Write a program which repeatedly reads numbers until the
user enters “done”. Once “done” is entered, print out the total, count,
and average of the numbers. If the user enters anything other than a
number, detect their mistake using try and except and print an error
message and skip to the next number.

Enter a number: 4
Enter a number: 5
Enter a number: bad data
Invalid input
Enter a number: 7
Enter a number: done
16 3 5.333333333333333
'''

message = ""
total = 0.0
count = 0
average = 0.0
print('\nPlease enter a number (or done to quit)')

while True:
    message = input('> ')
    if message == 'done':
        print('You entered:\n' + str(count) + ' numbers\n' + 'total: ' + str(total) + '\naverage: ' + str(average))
        print('Goodbye....')
        break
    try:
        message = float(message)
        total = total + message
        count = count + 1
        average = total / count

    except ValueError:
        print('invalid input')
