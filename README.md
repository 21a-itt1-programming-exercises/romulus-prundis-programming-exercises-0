# Romulus-Prundis-programming-exercises 0

In this repo you can find the Exercises from the Book "Python for everybody" by Dr. Charles R. Severance.

For writing the programs we need to have Python3.

The book was used for the subject Programming in the first semester at UCL (IT Technology)
18.01.2022.
