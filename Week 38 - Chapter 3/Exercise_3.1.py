#Exercise 3.1: Rewrite your pay computation to give the employee 1.5 times the hourly rate for hours worked above 40 hours.
#Enter Hours: 45, Enter Rate: 10, Pay: 475.0

hours_worked = float(input("Enter how many hours you worked \n"))
rate_salary = float(input("enter your salary rate \n"))

if hours_worked > 40:
    overtime = hours_worked - 40
    pay = 40 * rate_salary
    pay += overtime * rate_salary * 1.5
else:
    pay = hours_worked * rate_salary
    
print("pay:" , round (pay,2))