#Exercise 2: Rewrite your pay program using try and except so that your program handles non-numeric input gracefully by printing a messageand exiting the program. The following shows two executions of the program:
#Enter Hours: 20, Enter Rate: nine
#Error, please enter numeric input, Enter Hours: forty, Error, please enter numeric input

hours = input("enter hours \n")
rate = input("enter rate \n")
       
try:
       hours = float(hours)
       rate = float(rate)
       pay = (hours * rate)
except:
       print("try again")