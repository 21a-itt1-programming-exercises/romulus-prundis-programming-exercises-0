# Import Chatterbot
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
from chatterbot.trainers import ListTtrainer

bot = ChatBot (
    'Al',
    logic_adapters = [
        'chatterbot.logic.BestMatch',
        'chatterbot.logic.TimeLogic.Adapter',
        'chatterbot.logc.MathematicalEvaluation']
    )

# training the Al, here we use from >> chatterbot.trainers import ListTtrainer
trainer = listTtrainer(bot)

train_Al = ([
    "Hi",
        "Hello",
        "How are you?",
        "I'm good,breaking my back in a shoe store",
        "fine, and you?",
        "I have a complaint"
        "What shoes do you want to buy?",
        "What's your shoe size?",
        "What kind of shoes and what colour sould they be?",
        "This is bad. Can''t eat, can''t sleep, can''t bury the wife in the backyard.",
        "I dont have to fall asleep after sex. I want to fall asleep after sex. I welcome the darkness.",
        "I’m so hungry I could eat a vegetable!",
        "I wonder what the poor people are doing…",
        "I’m a shoe man, born and bred, dammit.",
        "Insurance is like marriage. You pay and pay, and never get anything back! Besides, the car isn’t worth more than 100 bucks with both kids in it.",
        "Do you want them delivered at home or to pick them up from the shop?",
        "What is your order number?"
])


# training the Al to know math
math_1 = [
    'pythagorean theorem',
    'a squared plus b squared equals c squared.'
]

math_2 = [
    'law of cosines',
    'c**2 = a**2 + b**2 - 2 * a * b * cos(gamma)'
]

list_trainer = ListTrainer(bot)


for item in (train_Al, math_1, math_2):
    list_trainer.train(item)

corpus_trainer.train("chatterbot.corpus.english")


name = input('What is your name: ')
print(f"Welcome {name} to the Al Bundy's shoe store! How can i help you? Life advices or shoes?")

response = bot.get_response("Can i help you with something else")
print("Bot response:", response)

while True:
    request = input(name+':')
    if request =='Bye' or request =='bye' or request =="that will be all":
        print('Al: See you')
        break
    else:
        response = bot.get_response(request)
        print('Bot:', response)







'''
print(my_bot.get_response("hi"))

print(my_bot.get_response("How i can help you"))

print(my_bot.get_response("Nice shoes you didn't bought it from me"))

print(my_bot.get_response("do you know the pythagorean theorem?"))
'''