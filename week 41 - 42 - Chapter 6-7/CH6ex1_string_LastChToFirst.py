"""
Exercise 1: Write a while loop that starts at the last character in the string
and works its way backwards to the first character in the string,
printing each letter on a separate line, except backwards.
"""

fruit = "vegetables"
index = len(fruit)     # len tells us the lenghts of the word vegetable

while index > 0 :
    print(fruit[index-1])    # by starting with -1 we start from the last letter of the word vegetables
    index -= 1
    