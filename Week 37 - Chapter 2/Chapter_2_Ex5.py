'''Exercise 5: Write a program which prompts the user for a Celsius temperature, convert the temperature to Fahrenheit, and print out the
converted temperature.'''

# Explaining the user what will happen
print("Welcome to the Celsius to Fahrenheit converter!")

# Communicating with the user / User input and storing his answer in the variable (celsius)
celsius = float(input("Write the temperature in celsius: "))

# Converting the celsius into fahrenheit and storing the result into a new variable (fahrenheit)
fahrenheit = float((celsius * 1.8) + 32.0)

# Printing the results
print(f"Your temperature in fahrenheit is {fahrenheit}°F")
