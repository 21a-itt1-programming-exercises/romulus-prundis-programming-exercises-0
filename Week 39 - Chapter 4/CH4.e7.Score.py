'''Exercise 7: Rewrite the grade program from the previous chapter using
a function called computegrade that takes a score as its parameter and
returns a grade as a string.
Score Grade
>= 0.9 A
>= 0.8 B
>= 0.7 C
>= 0.6 D
< 0.6 F'''

try:
    score = input("Write a number between 0.0 and 1.0: \n")
    s = float(score)

except:
    print("Your number wasn't between the range, try again")
finally:
    print("Let's try")

def computergrade():
    if 0.0 < s >= 1.0 :
        print("Bad score")
    elif s >= 0.9 :
        print("Perfect, A")
    elif s >= 0.8 :
        print("Good job, B")
    elif s >= 0.7 :
        print("Decent, C")
    elif s >= 0.6 :
        print(" Still ok, D")
    elif s < 0.6 :
        print("Doesn't look good, F")
computergrade()

