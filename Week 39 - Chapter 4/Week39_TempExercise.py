import machine
import utime

sensor_temp = machine.ADC(4)
conversion_factor = 3.3 / (65535)

temp = []
file = open ("temps.text", "w")

while True:
    reading = sensor_temp.read_u16() * conversion_factor
    
    # The temperature sensor measures the Vbe voltage of a biased bipolar diode, connected to the fifth ADC channel
    # Typically, Vbe = 0.706V at 27 degrees C, with a slope of -1.721mV (0.001721) per degree. 
    temperature = 27 - (reading - 0.706)/0.001721
    print("Your temperature is " + str(temperature))
    temps.append(temperature)
    avg_temp = sum(temps)/len(temps)
    file.write(str(temperature) + "\n")
    file.flush()
    print("average temperature this session is " + str(avg_temp))
    utime.sleep(2)
#To display file after stopping, type in shell file = open ("temps.text")
    print(file.read())
    
