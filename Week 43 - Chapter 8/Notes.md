
# 1.How do you create a list ?
list = []
# 2.What element types can a list hold ?
any elements
# 3.What does it mean, that lists are mutable ?
You can change the order of items in a list,  but also the items in the list.
# 4.How do you traverse a list ?
with a for loop

for cheese in cheeses:
    print(cheese)

# 5.How can you concatenate lists ?
a = [...]
b = [..]
conc = a + b

list1 = ['1', '2', '3']
list2 = ['4', '5', '6']
list3 = list1.append[list2]

and with the "append" function
# 6.What is the output of [7,6,5] * 2
[7,6,5,7,6,5]
# 7.How do you slice a list ?
[2:3]
# 8.What is the return value of the pop list method ?
pop modifies the list and returns the element that was removed.
# 9.How do you sum a list of numbers ?
list = [1,2,3]
print(sum(list))
# 10.If you have multiple variable assigned to the same list, what impact will it have to change one of the variables ?
If you change one of them it will change the rest
