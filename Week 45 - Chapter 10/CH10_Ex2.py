"""
Exercise 2: This program counts the distribution of the hour of the day
for each of the messages. You can pull the hour from the “From” line
by finding the time string and then splitting that string into parts using
the colon character. Once you have accumulated the counts for each
hour, print out the counts, one per line, sorted by hour as shown below
"""

#import pprint
#Creating dictionary 
filename = input('Please enter file name\n')
fhand = open(filename)

hours_of_day = {}
for line in fhand:
    if line.startswith('From '):
        line = line.split()
        time = line[5]
        hours = time[0:2]
        hours_of_day[hours] = hours_of_day.get(hours,0) + 1

#Sorting Dictionary
lst = list()
for key, val in list(hours_of_day.items()):
    lst.append((key, val))
lst.sort()
print(lst)
