"""
Exercise 1: Revise a previous program as follows: Read and parse the
“From” lines and pull out the addresses from the line. Count the num-
ber of messages from each person using a dictionary.
After all the data has been read, print the person with the most commits
by creating a list of (count, email) tuples from the dictionary. Then
sort the list in reverse order and print out the person who has the most
commits.
Sample Line:
From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008
Enter a file name: mbox-short.txt
cwen@iupui.edu 5
Enter a file name: mbox.txt
zqian@umich.edu 195
"""

file_name = input("Enter file name: ")
fhand = open(file_name, "r")
email_add = {}

for line in fhand:
    if line.startswith("From "):
        email = line.split()[1]
        email_add[email] = email_add.get(email, 0) + 1

first = []
for key, value in email_add.items():
    first.append((value,key))

first.sort(reverse=True)
p_tuple = first[0]

print(p_tuple[1], p_tuple[0])
